"""
Copyright (C) 2014, Zoomer Analytics LLC.
All rights reserved.

Version: 0.1.0
License: BSD 3-clause (see LICENSE.txt for details)
"""
from xlwings import Workbook, Range

wb = Workbook()  # Create a reference to the calling Excel Workbook


def fibonacci(n):
    """
    Generates the first n Fibonacci numbers.
    """
    first = 0
    second = 1
    seq = [first, second]
    for i in xrange(n - 1):
        tot = first + second
        seq.append(tot)
        first = second
        second = tot
    return map(str, seq[:n])


def xl_fibonacci():
    """
    This is a wrapper around fibonacci() to handle all the Excel stuff
    """
    # Get the input from Excel and turn into integer
    n = int(Range('B1').value)

    # Call the main function
    seq = fibonacci(n)

    # Clear output
    Range('C1').vertical.clear_contents()

    # Return the output to Excel - turn into list of list for column orientation
    Range('C1').value = [list(i) for i in zip(seq)]

if __name__ == "__main__":
    xl_fibonacci()
