# MFE 9815<p>Software Engineering in Finance<br/>Breman Thuraisingham _Morgan Stanley_<br/>Alain Ledon _JP Morgan Chase_</p>

---

# Today We'll Cover

* Batteries Included
* Python Language
* Python Shells
* Process Data from Yahoo! Finance

---

# Batteries Included

![monty-python](monty-python.jpg)

How do I serve files from current folder via HTTP?

    !bash
    $ python -m SimpleHTTPServer 8080

---

# An Interactive Shell

## Follow Along with the bpython shell

    !bash
    $ sudo apt-get install bpython
    # start with bpython
    $ bpython

## You'll get immediate feedback

![bpython](bpython-sm.png)

---

# Python Type System*

.notes: [Python website](http://www.python.org)

## Python is an extremely versatile, multi-paradigm language

It is dynamic - _variables may be declared without a type_

    !python
    mystr = "this is a string"
    mynum = 213

Typing is strong - _restricts how operations involving different can be combined_

    !python
    >>> a = 123
    >>> b = "a string"
    >>> a + b
    Traceback (most recent call last):
      File "<input>", line 1, in <module>
      TypeError: unsupported operand type(s) for +: 'int' and 'str'


Python is case-sensitive `myvar` and `MYVAR` are distinct.

    !python
    >>> myvar = 23
    >>> MYVAR = 3
    >>> print myvar + MYVAR
    26

---

# Comparison of Type Systems*

.notes: [Type systems](http://en.wikipedia.org/wiki/Type_system) 

## C++ and Java
<font color="maroon">Static and Strong</font>

## Python
<font color="navy">Dynamic and Strong</font>

## Bash and Javascript
<font color="green">Dynamic and Weak</font>

## Our selection of languages was not arbitrary.

Large systems with large teams tend to use staticly typed languages.

_But you will rarely build large, complex system._

More often, you'll _interact_ with large, complex systems:

* Operating System and Libraries
* Servers on LAN and Internet

---

# Help

## Documentation available

    !python
    >>> import os
    >>> help(os)
    ...[ help output ]...

## Query an object's interface with

    !python
    >>> dir(os)
    ['EX_CANTCREAT', 'EX_CONFIG', 'EX_DATAERR', ... ] 

## You can also query an object's docstring

    !python
    >>> import os
    >>> os.__doc__
    "OS routines for Mac, NT, or Posix depending ... "

---

# Syntax

## Python is extremely readable

<font color="red">60% to 80% of software cost is in maintenance.</font>

## No mandatory statement termination characters

    !python
    myval = 3 + 6

_No terminating semicolon_

## Lexical scope (blocks) specified by indentation

    !python
    class Payment(object):
        def __init__(self):
            """
            A Multiline comment
            With additional documentation
            """
            self.amount = 1000  # Note no terminal semicolon
            self.currency = 'USD'   # Scope is indented

* One line comments begin with `#` 
* Multi-line comments begin with `"""` (single or double quotes)

---

# Data Types

Python comes with a rich collection of data structures

## Lists and Tuples (Immutable Lists)

Python lists may contain any type including other lists.

    !python
    >>> fruit = [ 'apple', 'banana' ]
    >>> color = [ 'red', 'yellow' ]
    >>> blend = [ fruit, color ]

Negative numbers count from the end. -1 is the last item.

    !python
    >>> mylist = [1,2,3,4,5]
    >>> mylist[0]
    1
    >>> mylist[-1]
    5

Tuples behave just like lists but are immutable.

    !python
    >>> mylist[0]=123
    >>> mylist
    [123, 2, 3, 4, 5]
    >>> mytuple = (1,2,3)
    >>> mytuple[0]=123
      TypeError: 'tuple' object does not support item assignment

---

# Data Types (cont'd)

## Slicing

Access slices of lists with the colon `:`

    !python
    >>> mylist=[1,2,3,4,5]
    >>> mylist[0:2]
    [1, 2]


Leave the start index blank to imply the first element, leave the end index blank to imply last.

    !python
    >>> mylist[:-1]
    [1, 2, 3, 4]


## Dictionaries

Dictionaries are _associative arrays_ also known as _hash tables_.

    !python
    >>> mydict = { "key1" : "value1", "key2" : "value2" }
    >>> mydict["key1"]
    'value1'
---

# Strings

## Quoting

Python strings can be single quoted, double quoted or triple quoted.

    !python
    >>> mystr = 'hello'
    >>> mystr2 = "hello"
    >>> mystr3 = "The computer said 'hello, world!'"

## Parameters

Parametrize string with `%` and tuples

    !python
    >>> greeting = "Hello, %s. We calculated the result %.3f" % ("Alice", 6.632)
    >>> print greeting
    Hello, Alice. We calculated the result 6.632

---

# Flow Control

Flow control statements include `if`, `for` and `while`. 

    !python
    >>> rangelist = range(10)
    >>> print rangelist
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    >>> help(range)

`range` supports _start_, _stop_, and _step_ parameters:

    !python
    >>> for i in range(3,10,2):
    ...     print i
    ... 
    3
    5
    7
    9

Python also supports _list comprehension_.

    !python
    >>> words = 'The quick brown fox jumps over the lazy dog'.split()
    >>> collection = [[w.upper(), w.lower(), len(w)] for w in words]
    >>> print collection
    [['THE', 'the', 3], 
     ['QUICK', 'quick', 5], 
     ['BROWN', 'brown', 5], ...

---

# Functions*

.notes: [Lambda functions](http://www.secnetix.de/olli/Python/lambda_functions.hawk)

Declare functions with the `def` keyword:

    !python
    >>> def square(val):
    ...     return val * val;
    ...     
    ... 
    >>> print square(32)
    1024

Parameters are passed by reference.

Python supports anonymous functions that can be created at runtime.

Lambda functions aren't quite the same as lambda in functional programming but they are powerful constructs in their own right.

    !python
    >>> def make_incrementor (n): return lambda x: x + n
    ... 
    >>> f = make_incrementor(25)
    >>> g = make_incrementor(100)
    >>> print f(42), g(42)
    67 142

---

# Classes

Python supports class-based inheritance (as opposed to prototype-based inheritance you'll find in JavaScript).

Access modifiers (public, private) are not enforced by the language. 

Instead, the common convention is to prepend double underscores to methods and properties that are private.

    !python
    >>> class MyClass(object):
    ...     common = 10
    ...     def __init__(self, myvar):
    ...         self.__myvar = myvar
    ...     def myfunction(self, arg1, arg2):
    ...         return self.__myvar + arg1 + arg2
    ... 
    >>> instance = MyClass(30)
    >>> instance.myfunction(34,20)
    84

## Duck Typing

Python's object exhibit a behavior called _duck typing_. 

"If walks like a duck, and quacks like a duck, it must be a duck."


---

# Exceptions

Exceptions in Python are handled with `try-except [exception name]` blocks.

    !python
    >>> def some_function(val):
    ...     try:
    ...         # division by zero is not allowed
    ...         return 10/val
    ...     except ZeroDivisionError:
    ...         print "Cannot divide by zero!"
    ...     else:
    ...         # we're ok
    ...         pass
    ...     finally:
    ...         # this block is always executed
    ...         # useful for ensuring resources are released
    ...         # e.g. database connections
    ...         print "All cleaned up!"
    ... 
    >>> some_function(5)
    All cleaned up!
    2
    >>> some_function(0)
    Cannot divide by zero!
    All cleaned up!

---

# Importing

Python has an enormous collection of libraries.

* NumPy - linear algebra, Fourier transform, and C++/Fortran integration
* SciPy - Extends NumPy with optimization, ODE solvers, signal processing
* Pandas - Practical, real-world statistical analysis

Remember to explore what others have written before writing something from scratch!

    !python
    >>> import random
    >>> randomint = random.randint(1,500)
    >>> print randomint
    141

---

# File I/O

Accessing the file system with Python is _easy_!

    !python
    >>> myfile = open("/tmp/data.txt","w")
    >>> myfile.write("this is some important data")
    >>> myfile.close()
    >>> # let's read it
    >>> readfile = open("/tmp/data.txt","r")
    >>> print readfile.readlines()
    ['this is some important data']
    >>> readfile.close()

---

# Project Layout

Python is structured around the notion of _packages_ and _modules_.

## Modules

A _module_ is any Python source file on your python library path (`PYTHONPATH`).

A file named `quant.py` corresponds to a module named `quant` and you write `import quant`.

## Packages

A _package_ is used to organize modules. Any folder on your Python path that contains a file called `__init__.py` is considered a package, and may contain modules or more packages (subfolders with an `__init__.py` file).

The `__init__.py` file can be empty or can be used to consolidate constants, methods and classes that are used throughout child modules.

    !python
    import quant.rates
    import quant.credit
    from quant.equity import capm

---

# Python Shells

.notes: [Read, evaluate, print loop](http://en.wikipedia.org/wiki/Read-eval-print_loop)

Python has two excellent shells for REPL (Read, Evaluate, Print loop).

## IPython

* Interactive data visualization (graphing)
* Parallel computing
* Extensive inline help system

## bpython 

* In-line syntax highlighting
* Autocomplete
* Parameter hints
* "Rewind" function
* Share snippets via Pastebin

--- 

# Yahoo Stock Data (Take 2)

---

# Clone the Demo Project

    !bash
    $ mkdir ${HOME}/hgdev/org.bitbucket/b9815
    $ cd ${HOME}/hgdev/org.bitbucket/b9815
    $ hg clone https://${USER}@bitbucket.org/b9815/python

<h2>You now have the demo source code installed locally</h2>

---

# Install Prerequisites

## You'll need to following packages

* mercurial
* meld
* python-matplotlib
* ipython

## Next

    !bash
    $ sudo apt-get install python-matplotlib
    $ sudo apt-get install ipython

---

# Work With Data

    !bash
    # start your ipython session
    $ cd ${HOME}/hgdev/org.bitbucket/b9815/python
    $ ./isession.sh

## Let's get Google's stock price

    !python
    In [1]: import ystockquote
    In [2]: ystockquote.get_price('GOOG')
    Out[2]: '525.56'

---

# Basic Visualization

## From within the same folder

    !bash
    $ cd ${HOME}/hgdev/org.bitbucket/b9815/python
    $ source env.sh
    $ python goog-chart.py

## A plot is generated!

![google chart](goog-chart-sm.png)

---

# More Advanced Visualization

## Linked Charts

    !python
    $ python linked-chart.py

## Our linked charts

![linked chart](linked-chart-sm.png)

---

# A Quick Website

## It's Easy to Share Your Results

    !bash
    $ cd website
    $ python -m SimpleHTTPServer 8080

Now point your browser to `http://localhost:8080`

![webite](website-sm.png)
