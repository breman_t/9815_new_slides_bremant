# MFE 9815<p>Software Engineering in Finance<br/>Class 5 - Python<br/>Norman Kabir _Autonomy Capital Research_<br/>Alain Ledon _Ally Financial_</p>

---
# Today We'll Cover

* Exceptions
* Unit Tests
* Factories

---
# Exceptions*

.notes: [Errors and Exceptions](http://docs.python.org/tutorial/errors.html)

* Exceptions indicate errors and break out of the normal control flow of a program. 
* An exception is raised using the **raise** statement. 
* If the raise statement is used by itself, the last exception generated is raised again (although this works only while handling a previously raised exception). 
* Exceptions are caught using **try** and **except** statements

Example:

    !python
    # raising an exception
    raise RuntimeError("The s... hit the fan")
    ...
    # catching an exception
    try:
        f = open('foo')
    except IOError as e:
        statements

---
# Exceptions (cont.)

* When an exception occurs, the interpreter stops executing statements in the try block and looks for an except clause that matches the exception
* If one is found, control is passed to the first statement in the except clause. 
* After the except clause is executed, control continues after the try-except block. 
* If not, the exception is propagated up to the block of code in which the try statement appeared. 
* If an exception works its way up to the top level of a program without being caught, the interpreter aborts with an error message. 
* The optional __as var__ modifier to the **except** statement supplies the name of a variable in which an instance of the exception
* Exception handlers can examine this value to find out more about the cause of the exception. 

---
# Exceptions (cont.)

Multiple exception handling blocks are specified using multiple **except** clauses

    !python
    try:
        # do stuff
        ...
    except IOError as e:
        # handle IOError
        ...
    except TypeError as e:
        # handle TypeError
        ...
    except NameError as e:
        # handle NameError
        ...
        
A single handler can catch multiple exceptions:

    !python
    try:
        # do stuff
        ...
    except (IOError, TypeError, NameError) as e:
        # handle all exceptions
        ...

---
# Exceptions (cont.)

Use **pass** to ignore an exception 

    !python
    try:
        # working code
        ...
    except IOError as e:
        pass
        
To catch all exceptions except those related to program exit, use **Exception**. Make sure to report accurate information so you can debug the issue later.

    !python
    try:
        # working code
        ...
    except Exception as e:
        # detailed report 
        ...

---
# Exceptions (cont.)

The **try** statement supports an **else** clause after the last **except**. It is executed if no exception is raised. You can re-raise by calling **raise** without argument

    !python
    try:
        f = open('myfile', 'r')
    except IOError as e:
        mylog.write('Unable to open myfile: {0}'.format(e))
        raise
    else:
        data = f.read()
        f.close
        
The **try** statement also supports a **finally** clause. It is executed regardless if there is an exception or not. The code in **finally** will always be executed regardless if there is an error or not (useful to manage resources, i.e. files).

    !python
    f = open('myfile', 'r')
    try:
        # do stuff
        ...
    finally:
        # close the file regardless
        f.close
        
[Python Exception Handling Techniques](http://www.doughellmann.com/articles/how-tos/python-exception-handling/index.html)
        
---
# Built-in Exceptions*

.notes: [Built-in Exceptions](http://docs.python.org/library/exceptions.html)

    BaseException  
     +-- SystemExit  
     +-- KeyboardInterrupt  
     +-- GeneratorExit  
     +-- Exception  
          +-- StopIteration  
          +-- StandardError  
          +-- Warning  
               +-- DeprecationWarning  
               +-- PendingDeprecationWarning  
               +-- RuntimeWarning  
               +-- SyntaxWarning  
               +-- UserWarning  
               +-- FutureWarning  
               +-- ImportWarning  
               +-- UnicodeWarning  
               +-- BytesWarning  
       
---
# Built-in Exceptions (cont.)

    StandardError  
        +-- BufferError  
        +-- ArithmeticError  
        |    +-- FloatingPointError  
        |    +-- OverflowError  
        |    +-- ZeroDivisionError  
        +-- AssertionError  
        +-- AttributeError  
        +-- EnvironmentError  
        |    +-- IOError  
        |    +-- OSError  
        |         +-- WindowsError (Windows)  
        |         +-- VMSError (VMS)  
        +-- EOFError  
        +-- ImportError  
        +-- LookupError  
        |    +-- IndexError  
        |    +-- KeyError  
        +-- MemoryError  
        +-- NameError  
        |    +-- UnboundLocalError  
        +-- ReferenceError  
        +-- RuntimeError  
        |    +-- NotImplementedError  
        +-- SyntaxError  
        |    +-- IndentationError  
        |         +-- TabError  
        +-- SystemError  
        +-- TypeError  
        +-- ValueError  
             +-- UnicodeError  
                  ...
      
---
# Defining New Exceptions*

.notes: [User-defined Exceptions](http://docs.python.org/tutorial/errors.html#user-defined-exceptions)

* All exceptions are defined in terms of classes
* Create a new exception as a new class that inherits from Exception
* Values supplied with the **raise** statement are used as arguments to the exception's constructor

Example:

    !python
    class DeviceError(Exception):
        def __init__(self, errno, msg):
            self.args = (errno, msg)
            self.errno = errno
            self.msg = msg
    ...
    raise DeviceError(22, 'GIPB Error')
    
---
# Test Driven Development (TDD)

## Something that is untested is broken.

![You need some tests](testing.jpg)

---
# Types of Testing

* **Unit Testing** - Test the smallest pieces of a program. Often, it means individual functions or methods. Make sure the unit tested works as expected.
* **Integration Testing** - Test interactions between related units. Check if tested units behave correctly as a group
* **System Testing** - Checks parts of the program after everything has been put together. They are like a extreme form of **Integration Testing**

We are going to cover **Unit Testing**

---
# Unit Tests

* Test functions/methods in isolation, independent units
* Find problems early in the development process
* Serve also as a living documentation for your code
* Tests life-cycle  
    - Write a test 
    - Watch it fail
    - Make it pass  (Refactor your code)
    - Rinse repeat  

---
# Unit Test Frameworks*

.notes: [Python Unit Testing Tools](http://pycheesecake.org/wiki/PythonTestingToolsTaxonomy#UnitTestingTools)

Various unit test frameworks available in python. 

* [unittest](http://docs.python.org/library/unittest.html)
* [nose](http://nose.readthedocs.org/en/latest/)
* doctest
* pytest

... and many more  

---
# unittest - Python built-in framework*

.notes: [Python unittest module](http://docs.python.org/library/unittest.html)

* Comes standard with Python
* Usually called PyUnit - Python version of JUnit Java test framework
* Supports test automation
* Based on 4 core concepts:
    * __Test Fixture__ - Preparation needed to perform one or more tests, and any associate cleanup actions
    * __Test Case__ - The smallest unit of testing. It checks for a specific response to a particular set of inputs
    * __Test Suite__ - Collection of test cases, test suites or both
    * __Test Runner__ - Executor of tests
* Test Case classes must inherit from **unittest.TestCase**
* Test functions and test modules should be named starting with **test**/**Test**

---
# unittest Example*

.notes: hg clone https://littlea1@bitbucket.org/littlea1/mfe9815python3

    !python
    # splitter.py - From Essential Python Reference
    def split(line, types=None, delimiter=None):
        """Splits a line of text and optionally performs type conversion
            ...
        """
        fields = line.split(delimiter)
        if types:
            fields = [ty(val) for ty, val in zip(types, fields)]
        return fields

---
# unittest Example (cont.)

    !python
    import splitter
    import unittest

    # Unit Tests
    class TestSplitFunction(unittest.TestCase):
        def setUp(self):
            # Perform set up (if any)
            pass
        def tearDown(self):
            # Perform clean up (if any)
            pass
        def test_simple_string(self):
            r = splitter.split('GOOG 100 701.50')
            self.assertEqual(r, ['GOOG', '100', '701.50'])
        def test_type_convert(self):
            r = splitter.split('GOOG 100 701.50', [str, int, float])
            self.assertEqual(r, ['GOOG', 100, 701.50])
        def test_delimiter(self):
            r = splitter.split('GOOG,100,701.50', delimiter=',')
            self.assertEqual(r, ['GOOG', '100', '701.50'])

    # Run the unittests
    if __name__ == '__main__':
        unittest.main()

---
# unittest functions

Some of the methods supported by **unittest.TestCase**:

* t.setUp
* t.tearDown
* t.assert_ / t.failIf
* t.assertEqual / t.failUnlessEqual
* t.assertNotEqual / t.failIfEqual
* t.assertAlmostEqual / t.failUnlessAlmostEqual
* t.assertRaises / t.failUnlessRaises

---
# Nosetests*

.notes: [An Extended Introduction to the nose Unit Testing Framework](http://ivory.idyll.org/articles/nose-intro.html)

* Popular Python unit test framework
* Compatible with **unittest**
* Support advance features 
* Discovers and runs unit tests
* Anything that matches **((?:^|[b_.-])[Tt]est)** will be considered a test (i.e. don't use the word test in any name)
* Doesn't need a super class

Installation and Run:

    !bash
    $ sudo pip install nose
    $ nosetests
    ...

---
# Nosetests

* Nose looks through a directory's structure, finds the test files, sorts out the tests that they contain, runs the tests, and reports the results back to you. 
* Nose recognizes test files based on their names. Any file whose name contains test or Test either at the beginning or following any of the characters _, . or -
* Any directory whose name matches the same pattern is recognized as a directory that might contain tests, and so should be searched for test files.

---
# Nosetests

Some of the methods supported by **nose.tools**:

* t.assert_equal
* t.assert_not_equal
* t.assert_almost_equal
* t.assert_raises
* ...

Methods are very similar to **unittest.TestCase** but with a PEP-08 name convention.

---
# Nosetests Example

.notes: hg clone https://littlea1@bitbucket.org/littlea1/mfe9815python3

    !python
    import splitter
    from nose.tools import *

    # Unit Tests
    class TestSplitFunction():
        @classmethod
        def setup_class(klass):
            """This method is run once for each class before any tests are run"""

        @classmethod
        def teardown_class(klass):
            """This method is run once for each class _after_ all tests are run"""
     
        def setUp(self):
            """This method is run once before _each_ test method is executed"""
     
        def teardown(self):
            """This method is run once after _each_ test method is executed"""
           
        def test_simple_string(self):
            r = splitter.split('GOOG 100 701.50')
            assert_equal(r, ['GOOG', '100', '701.50'])
        
        def test_type_convert(self):
            r = splitter.split('GOOG 100 701.50', [str, int, float])
            assert_equal(r, ['GOOG', 100, 701.50])
        
        def test_delimiter(self):
            r = splitter.split('GOOG,100,701.50', delimiter=',')
            assert_equal(r, ['GOOG', '100', '701.50'])

--- 
# Nosetests

* Expandable by using plugins
* Supports test coverage by using coverage
* Supports profiling
* Supports debugging of tests online
* Supports parallel testing
* ... and much more (__$ nosetests --help__)

---
# Nosetests References

* [http://nose.readthedocs.org/en/latest/](http://nose.readthedocs.org/en/latest/)
* [http://nose.readthedocs.org/en/latest/writing_tests.html](http://nose.readthedocs.org/en/latest/writing_tests.html)
* [http://ivory.idyll.org/articles/nose-intro.html](http://ivory.idyll.org/articles/nose-intro.html)
* [http://bit.ly/QI0RS4](http://bit.ly/QI0RS4)

---
# Factory Pattern

* Object-oriented creational design pattern 
* Control the object creation process
* Perform extra tasks needed in creating instances of an object

---
# Example*

.notes: hg clone https://littlea1@bitbucket.org/littlea1/mfe9815python3

    !python
    from shape import Shape

    class Circle(Shape):

        def __init__(self, name, radius):
            
            super(Circle, self).__init__(name)
            self.radius = radius

        def area(self):
            return 3.14159 * self.radius
            
        def anything_else(self):
            print "I have area = {0}".format(area)

        def __str__(self):
            return "Circle with name {0} and radius {1}".format(self.name, self.radius)

    def make(name, radius):
        if radius < 0:
            raise ValueError("Radius most be greater or equal to 0")
        try:
            r = float(radius)
            return Circle(name, float(radius))
        except ValueError as e:
            raise ValueError("Radius most be a number.")
        
    def make_empty(name):
        return Circle(name, 0.)

---
# More

    !python
    In [1]: import circle

    In [2]: c = circle.make("blue circle", 4.0)

    In [3]: print c
    Circle with name blue circle and radius 4.0

    In [4]: r = circle.make_empty("red circle")

    In [5]: print r
    Circle with name red circle and radius 0.0

    In [6]:

    


