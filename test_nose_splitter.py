import splitter
from nose.tools import *

# Unit Tests
class TestSplitFunction():
    @classmethod
    def setup_class(klass):
        """This method is run once for each class before any tests are run"""
        pass
    
    @classmethod
    def teardown_class(klass):
        """This method is run once for each class _after_ all tests are run"""
        pass
    
    def setUp(self):
        """This method is run once before _each_ test method is executed"""
        pass
    
    def teardown(self):
        """This method is run once after _each_ test method is executed"""
        pass
    
    def test_simple_string(self):
        r = splitter.split('GOOG 100 701.50')
        assert_equal(r, ['GOOG', '100', '701.50'])
    
    def test_type_convert(self):
        r = splitter.split('GOOG 100 701.50', [str, int, float])
        assert_equal(r, ['GOOG', 100, 701.50])
        
    def test_delimiter(self):
        r = splitter.split('GOOG,100,701.50', delimiter=',')
        assert_equal(r, ['GOOG', '100', '701.50'])
    