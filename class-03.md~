# MFE 9815<p>Software Engineering in Finance<br/>Breman Thuraisingham _Morgan Stanley_<br/>Alain Ledon _JP Morgan Chase_</p>

---

# Today We'll Cover

* Unix Shell Scripting
* Git
* Batteries Included
* Introduction to Python Language
* Python Shells

---

# Change permissions 'chmod' *

.notes: [https://kb.iu.edu/d/abdb](https://kb.iu.edu/d/abdb)

In Linux permissions are determined by Access control. You can restrict reading/writing or execution persimission to a file for a user or a group or users not in the group or all. 
The command to do this is `chmod`.

Examples:

    !bash
    chmod +x script.sh
    
makes script.sh *executable*. It assumes all.

    !bash
    chmod a+w script.sh
    
gives every user *write* permission to script.sh.

---

# Shell Scripting

You can combine multiple shell commands in a script to programmatically execute them in one pass. The file with all the commands is usually called shell script. By convention we put extension _.sh_ or _.bsh_ or _.ksh_ 

The first line must be the shell you are using

    !bash
    #!/bin/bash
    
    VAR1="Alain" 
    VAR2="Ledon"
    
    echo "this is a script" $VAR1 ${VAR2}
    pwd
    ls -1

    CURR_DIR=$(pwd)
    echo "This is the current directory = " ${CURR_DIR}

---

# Variables

You can use variables in your scripts. There is no need to declare the variables. They are dynamic in nature. The shell determines the type automatically.

These variables exist only while the script runs. If you want to persist the variable in the environment, you will need to use the keyword _export_.

User variables can be referenced using the dollar sign _$_ or with the dollar sign and curly braces _${...}_

    !bash
    #!/bin/bash
    
    ONE=1
    ONE_TOO=$ONE
    
    FIRST="Alain"
    LAST="LEDON"
    LAST2=${LAST}
    
    export FULL_NAME="Alain Ledon"


---

# More about Variables

Linux keeps a set of standard environment variables with specific meaning.

* _$$_ = Process ID of shell
* _$?_ = Exit status of last command.
* _$\__ = Name of last command.
* _$PATH_ = A colon-delimited list of locations where trusted executables are installed. Any executable in one of these locations can be executed without specifying a complete path.
* _$IFS_ = Input Field Separators
* _$HOME_ = The user’s home directory.
* _$UID_ = The user’s ID.
* _$USER_ The user’s (short) login name.
* _$#_ = Number of arguments passed to the shell.
* _$@_ = Complete list of arguments passed to the shell, separated by spaces.
* _$*_ = Complete list of arguments passed to the shell, separated by the first character of the IFS (input field separators) variable.

---
# The backtick / $(...) assignment

The backtick (_\`_) allows you to assign the output of a shell command to a variable. While this doesn’t seem like much, it is a major building block in script programming. 

You must surround the entire command line command with backtick characters _\`<command\>\`_. The same effect can be achieved using _$(<command\>)_

    !bash
    #!/bin/bash
    
    # Returns todays date
    TODAY=`date +%y%m%d`
    
    # count number of lines in some CSV file
    LINES=$(wc -l some_data_file.csv)

    echo "Today is " ${TODAY}
    echo "Line count " $LINES
        
---
# For loops*

.notes: [http://www.freeos.com/guides/lsst/ch03sec06.html](http://www.freeos.com/guides/lsst/ch03sec06.html)

The bash shell provides the _for_ command to allow you to create a loop that iterates through a series of values.

    !bash
    for var in list
    do
        commands
    done

It is most commonly used to iterate through a list of files

    !bash
    # display the number of line per file
    for myfile in $(ls -1 *.csv)
    do
        LINES=$(wc -l myfile)
        echo ${myfile} ${LINES}
    done

---
# Conditionals*

.notes: [http://www.freeos.com/guides/lsst/ch03sec01.html](http://www.freeos.com/guides/lsst/ch03sec01.html)

Shell scripts support conditions by using _if_. However, they work a little different.

    !bash
    if command
    then
        command1
        command2
        ...
    else
        command3
        command4
        ...
    fi

* The bash shell _if_ statement runs the command defined on the _if_ line. 
* If the exit status of the command is zero (the command completed successfully), the commands listed under the _then_ section are executed. 
* If the exit status of the command is anything else, the _then_ commands aren’t executed. If there is an _else_ part, those commands are executed.
* You don't have to include an else part. The _fi_ keyword closes the _if_ statement

---
# Conditionals*

.notes: [http://www.freeos.com/guides/lsst/ch03sec01.html](http://www.freeos.com/guides/lsst/ch03sec01.html)

Since we need a comparison command, the shell provides the command _test_ or _[...]_. It is used to see if an expression is true, and if it is true it returns zero(0), otherwise returns nonzero for false.

    !bash
    # checks if a value passed as parameter is positive
    if test $1 -gt 0
    then
        echo "$1 number is positive"
    fi
    
    # does exactly the same as above but uses the shortcut [...] instead of test
    if [ $1 -gt 0 ]
    then
        echo "$1 number is positive"
    fi

There are shortcuts for comparison of numbers (_-eq, -ne, -gt, -lt_, etc), comparison of strings and test for files. Check the following link for all the details:

[http://www.freeos.com/guides/lsst/ch03sec02.html](http://www.freeos.com/guides/lsst/ch03sec02.html)

---
# References 

* [LinuxCommand.org](http://linuxcommand.org/)
* [Linux Shell Scripting Tutorial](http://www.freeos.com/guides/lsst/)
* [BASH Programming](http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO.html)
* [Bash Guide for Beginners](http://www.tldp.org/LDP/Bash-Beginners-Guide/html/)
* [Unix Power Tools](http://goo.gl/eLhvo6)
* [Linux Shell Scripting Cookbook](http://amzn.com/1782162747)
* [Data Science at the Command Line: Facing the Future with Time-Tested Tools](http://amzn.com/1491947853)

---

# Git

---

# Git Common Workflow

---

# git clone

---

# git add

---

# git commit

---

# git push

---

# git pull

---
# Batteries Included

![monty-python](monty-python.jpg)

How do I serve files from current folder via HTTP?

    !bash
    $ python -m SimpleHTTPServer 8080

---

# An Interactive Shell

## Follow Along with the bpython shell

    !bash
    $ sudo apt-get install bpython
    # start with bpython
    $ bpython

## You'll get immediate feedback

![bpython](bpython-sm.png)

---

# Python Type System*

.notes: [Python website](http://www.python.org)

## Python is an extremely versatile, multi-paradigm language

It is dynamic - _variables may be declared without a type_

    !python
    mystr = "this is a string"
    mynum = 213

Typing is strong - _restricts how operations involving different can be combined_

    !python
    >>> a = 123
    >>> b = "a string"
    >>> a + b
    Traceback (most recent call last):
      File "<input>", line 1, in <module>
      TypeError: unsupported operand type(s) for +: 'int' and 'str'


Python is case-sensitive `myvar` and `MYVAR` are distinct.

    !python
    >>> myvar = 23
    >>> MYVAR = 3
    >>> print myvar + MYVAR
    26

---

# Comparison of Type Systems*

.notes: [Type systems](http://en.wikipedia.org/wiki/Type_system) 

## C++ and Java
<font color="maroon">Static and Strong</font>

## Python
<font color="navy">Dynamic and Strong</font>

## Bash and Javascript
<font color="green">Dynamic and Weak</font>

## Our selection of languages was not arbitrary.

Large systems with large teams tend to use staticly typed languages.

_But you will rarely build large, complex system._

More often, you'll _interact_ with large, complex systems:

* Operating System and Libraries
* Servers on LAN and Internet

---

# Help

## Documentation available

    !python
    >>> import os
    >>> help(os)
    ...[ help output ]...

## Query an object's interface with

    !python
    >>> dir(os)
    ['EX_CANTCREAT', 'EX_CONFIG', 'EX_DATAERR', ... ] 

## You can also query an object's docstring

    !python
    >>> import os
    >>> os.__doc__
    "OS routines for Mac, NT, or Posix depending ... "

---

# Syntax

## Python is extremely readable

<font color="red">60% to 80% of software cost is in maintenance.</font>

## No mandatory statement termination characters

    !python
    myval = 3 + 6

_No terminating semicolon_

## Lexical scope (blocks) specified by indentation

    !python
    class Payment(object):
        def __init__(self):
            """
            A Multiline comment
            With additional documentation
            """
            self.amount = 1000  # Note no terminal semicolon
            self.currency = 'USD'   # Scope is indented

* One line comments begin with `#` 
* Multi-line comments begin with `"""` (single or double quotes)

---

# Data Types

Python comes with a rich collection of data structures

## Lists and Tuples (Immutable Lists)

Python lists may contain any type including other lists.

    !python
    >>> fruit = [ 'apple', 'banana' ]
    >>> color = [ 'red', 'yellow' ]
    >>> blend = [ fruit, color ]

Negative numbers count from the end. -1 is the last item.

    !python
    >>> mylist = [1,2,3,4,5]
    >>> mylist[0]
    1
    >>> mylist[-1]
    5

Tuples behave just like lists but are immutable.

    !python
    >>> mylist[0]=123
    >>> mylist
    [123, 2, 3, 4, 5]
    >>> mytuple = (1,2,3)
    >>> mytuple[0]=123
      TypeError: 'tuple' object does not support item assignment

---

# Data Types (cont'd)

## Slicing

Access slices of lists with the colon `:`

    !python
    >>> mylist=[1,2,3,4,5]
    >>> mylist[0:2]
    [1, 2]


Leave the start index blank to imply the first element, leave the end index blank to imply last.

    !python
    >>> mylist[:-1]
    [1, 2, 3, 4]


## Dictionaries

Dictionaries are _associative arrays_ also known as _hash tables_.

    !python
    >>> mydict = { "key1" : "value1", "key2" : "value2" }
    >>> mydict["key1"]
    'value1'
---

# Strings

## Quoting

Python strings can be single quoted, double quoted or triple quoted.

    !python
    >>> mystr = 'hello'
    >>> mystr2 = "hello"
    >>> mystr3 = "The computer said 'hello, world!'"

## Parameters

Parametrize string with `%` and tuples

    !python
    >>> greeting = "Hello, %s. We calculated the result %.3f" % ("Alice", 6.632)
    >>> print greeting
    Hello, Alice. We calculated the result 6.632

---

# Flow Control

Flow control statements include `if`, `for` and `while`. 

    !python
    >>> rangelist = range(10)
    >>> print rangelist
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    >>> help(range)

`range` supports _start_, _stop_, and _step_ parameters:

    !python
    >>> for i in range(3,10,2):
    ...     print i
    ... 
    3
    5
    7
    9

Python also supports _list comprehension_.

    !python
    >>> words = 'The quick brown fox jumps over the lazy dog'.split()
    >>> collection = [[w.upper(), w.lower(), len(w)] for w in words]
    >>> print collection
    [['THE', 'the', 3], 
     ['QUICK', 'quick', 5], 
     ['BROWN', 'brown', 5], ...

---

# Functions*

.notes: [Lambda functions](http://www.secnetix.de/olli/Python/lambda_functions.hawk)

Declare functions with the `def` keyword:

    !python
    >>> def square(val):
    ...     return val * val;
    ... 
    >>> print square(32)
    1024

Parameters are passed by reference.

Python supports anonymous functions that can be created at runtime.

Lambda functions aren't quite the same as lambda in functional programming but they are powerful constructs in their own right.

    !python
    >>> def make_incrementor (n): return lambda x: x + n
    ... 
    >>> f = make_incrementor(25)
    >>> g = make_incrementor(100)
    >>> print f(42), g(42)
    67 142

---

# Classes

Python supports class-based inheritance (as opposed to prototype-based inheritance you'll find in JavaScript).

Access modifiers (public, private) are not enforced by the language. 

Instead, the common convention is to prepend double underscores to methods and properties that are private.

    !python
    >>> class MyClass(object):
    ...     common = 10
    ...     def __init__(self, myvar):
    ...         self.__myvar = myvar
    ...     def myfunction(self, arg1, arg2):
    ...         return self.__myvar + arg1 + arg2
    ... 
    >>> instance = MyClass(30)
    >>> instance.myfunction(34,20)
    84

## Duck Typing

Python's object exhibit a behavior called _duck typing_. 

"If walks like a duck, and quacks like a duck, it must be a duck."

---

# Exceptions

Exceptions in Python are handled with `try-except [exception name]` blocks.

    !python
    >>> def some_function(val):
    ...     try:
    ...         # division by zero is not allowed
    ...         return 10/val
    ...     except ZeroDivisionError:
    ...         print "Cannot divide by zero!"
    ...     else:
    ...         # we're ok
    ...         pass
    ...     finally:
    ...         # this block is always executed
    ...         # useful for ensuring resources are released
    ...         # e.g. database connections
    ...         print "All cleaned up!"
    ... 
    >>> some_function(5)
    All cleaned up!
    2
    >>> some_function(0)
    Cannot divide by zero!
    All cleaned up!

---

# Importing

Python has an enormous collection of libraries.

* NumPy - linear algebra, Fourier transform, and C++/Fortran integration
* SciPy - Extends NumPy with optimization, ODE solvers, signal processing
* Pandas - Practical, real-world statistical analysis

Remember to explore what others have written before writing something from scratch!

    !python
    >>> import random
    >>> randomint = random.randint(1,500)
    >>> print randomint
    141

---

# File I/O

Accessing the file system with Python is _easy_!

    !python
    >>> myfile = open("/tmp/data.txt","w")
    >>> myfile.write("this is some important data")
    >>> myfile.close()
    >>> # let's read it
    >>> readfile = open("/tmp/data.txt","r")
    >>> print readfile.readlines()
    ['this is some important data']
    >>> readfile.close()

---

# Project Layout

Python is structured around the notion of _packages_ and _modules_.

## Modules

A _module_ is any Python source file on your python library path (`PYTHONPATH`).

A file named `quant.py` corresponds to a module named `quant` and you write `import quant`.

## Packages

A _package_ is used to organize modules. Any folder on your Python path that contains a file called `__init__.py` is considered a package, and may contain modules or more packages (subfolders with an `__init__.py` file).

The `__init__.py` file can be empty or can be used to consolidate constants, methods and classes that are used throughout child modules.

    !python
    import quant.rates
    import quant.credit
    from quant.equity import capm

---

# Python Shells*

.notes: [Read, evaluate, print loop](http://en.wikipedia.org/wiki/Read-eval-print_loop)

Python has two excellent shells for REPL (Read, Evaluate, Print loop).

## IPython

* Interactive data visualization (graphing)
* Parallel computing
* Extensive inline help system
* De-facto standard for data analytics via IPython Notebooks ([Project Jupyter](https://jupyter.org/))

## bpython 

* In-line syntax highlighting
* Autocomplete
* Parameter hints
* "Rewind" function
* Share snippets via Pastebin
* Not as used as IPython

--- 
# Install Prerequisites

Install Anaconda distribution and start exploring:

[https://store.continuum.io/cshop/anaconda/](https://store.continuum.io/cshop/anaconda/)

---

# A Quick Website

## It's Easy to Share Your Results

    !bash
    $ cd website
    $ python -m SimpleHTTPServer 8080

Now point your browser to `http://localhost:8080`

![webite](website-sm.png)
