# MFE 9815<p>Software Engineering in Finance<br/><br/>Alain Ledon</p>

---

# Design Patterns

---
# Design Patterns*

.notes: [from Gamma et. al.](http://amzn.com/0201633612)

## What is a Design Pattern?

A **design pattern** names, abstracts, and identifies the key aspects of a common design structure for creating a reusable object-oriented design. The design pattern identifies the participating classes and instances, their roles and collaborations, and the distribution of responsibilities. Each design pattern focuses on a particular object-oriented design problem or issue, when it applies, whether it can be applied in view of other design constraints, and the consequences and trade-offs of its use. 

1. The **pattern name** is a handle we can use to describe a design problem, its solutions, and consequences in a word or two. 
2. The **problem** describes when to apply the pattern. It explains the problem and its context.
3. The **solution** describes the elements that make up the design, their relationships, responsibilities, and collaborations. The solution doesn't describe a particular concrete design or implementation but an abstract description of a design problem and how a pattern solves it.
4. The **consequences** are the results and trade-offs of applying the pattern. The consequences of a pattern include its impact on a system's flexibility, extensibility, or portability. 

---
# Design Pattern Categories

* **Creational**: ways and means of object instantiation
* **Structural**: mutual composition of classes or objects (the Facade DP is Structural)
* **Behavioral**: how classes or objects interact and distribute responsibilities among them
* Each can be class-level or object-level

---
# Original Design Patterns

<table width="100%" border="0">
<tr><td><h2>Creational Patterns</h2></td>
    <td><h2>Structural Patterns</h2></td>
    <td><h2>Behavioral Patterns</h2></td>
    </tr>
<tr><td valign="top">
      <ul><li>Abstract Factory</li>
          <li>Builder</li>
	  <li>Factory Method</li>
	  <li>Prototype</li>
	  <li>Singleton</li>
      </ul>
    </td>
    <td valign="top"><ul><li>Adapter</li>
          <li>Bridge</li>
	  <li>Composite</li>
	  <li>Decorator</li>
	  <li>Facade</li>
	  <li>Flyweight</li>
	  <li>Proxy</li>
      </ul></td>
    <td valign="top"><ul><li>Chain of Responsibility</li>
          <li>Command</li>
	  <li>Interpreter</li>
	  <li>Iterator</li>
	  <li>Mediator</li>
	  <li>Memento</li>
	  <li>Observer</li>
	  <li>State</li>
	  <li>Strategy</li>
	  <li>Template Method</li>
	  <li>Visitor</li>
      </ul></td>
    </tr>
</table>

---
# Singleton Pattern

The **Singleton** pattern is a design pattern used to implement the mathematical concept of a singleton, by restricting the instantiation of a class to one object. This is useful when exactly one object is needed to coordinate actions across the system. 

The concept is sometimes generalized to systems that operate more efficiently when only one object exists, or that restrict the instantiation to a certain number of objects. Also, they should be used with care in multithreaded environments.

There are criticism to the use of the singleton pattern because it is overused, introduces unnecessary restrictions in situations where a sole instance of a class is not actually required, and introduces global state into an application.   ([Singletons are Evil](http://c2.com/cgi/wiki?SingletonsAreEvil "Singletons are Evil"))

However, **Singletons** are often preferred to global variables because:

* They don't pollute the global or containing name space with unnecessary variables.
* They permit lazy allocation and initialization, whereas global variables in many languages will always consume resources.

---
# Singleton Pattern UML

![Singleton UML] (http://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Singleton_UML_class_diagram.svg/500px-Singleton_UML_class_diagram.svg.png)


---
# Singleton Implementation in C++

    !cpp
    class Singleton {
    
    public:
      static Singleton& GetInstance();
      
    private:
      static Singleton* psingle;
      Singleton();
      Singleton( const Singleton& );
      Singleton& operator=( const Singleton& );
    }; 
    
    Singleton& Singleton::GetInstance() {
    
      if( !psingle ) {
        psingle = new Singleton;
      }
      return *psingle;
    }    
    
---
# Singleton Implementation in Python*
.notes: [from Google Presentation](http://www.aleax.it/gdd_pydp.pdf)

## Goal: We want just one instance to exist

* Use a module instead of a class
    * no subclassing, no special methods, ...
* make just 1 instance (no enforcement)
    * need to commit to "when" to make it
* First version: singleton ("highlander")
    * subclassing not really smooth
* Second version: monostate ("borg")
    * Guido dislikes it
    
---
# Singleton (Borg/Monostate) in Python

In summary, all datamembers are static and hence, shared among all instances. It has a nice implementation in python however it could lead to all sorts of problems in multithreaded environments

    !python
    class Borg:
        __shared_state = {}
    def __init__(self):
        self.__dict__ = self.__shared_state
    # and whatever else you want in your class -- that's all!
    
---
# Singleton Implementation in Python

Here is another solution. To prevent more than one instance, we raise an exception. All instances should be obtained by calling <em>GetInstance()</em>.

    !python
    class Singleton:
        __single = None
        def __init__( self ):
            if Singleton.__single:
                raise Singleton.__single
            Singleton.__single = self 
            
    def GetInstance(x = Singleton):
        try:
            single = x()
        except Singleton, s:
            single = s
        return single        
    
    class Child( Singleton ):
        def __init__( self, name ):
            Singleton.__init__( self )
            self.__name = name
        def name( self ):
            return self.__name
    
    class Junior( Singleton ):
        pass

---
# Singleton Implementation in Python

Here are the results:

    !python
    >>> child = Child('Monty')
    >>> junior = GetInstance(Junior)
    >>> junior.name()
    'Monty'
    >>> single = GetInstance()
    >>> single
    <Child instance at d1d88>        
    
---        
# Adapter (aka Wrapper) Pattern

The **Adapter** pattern will convert the interface of a class into another interface clients expect. Adapter lets classes work together that couldn't otherwise because of incompatible interfaces. The Adapter changes the interface of the Service, but preserves it behavior.

This is sometimes called a wrapper because an adapter class wraps the implementation of another class in the desired interface. This pattern makes heavy use of delegation wher the delegator is the adapter (or wrapper) and the delegate is the class being adapted.

## Use the Adapter pattern when:

* You want to use an existing class, and its interface does not match the one you need.
* You want to create a reusable class that cooperates with unrelated or unforeseen classes, that is, classes that don't necessarily have compatible interfaces.
* You need to use several existing subclasses, but it's impractical to adapt their interface by subclassing every one. An object adapter can adapt the interface of its parent class.
---        
# Adapter (aka Wrapper) Pattern

![Adapter Pattern](http://upload.wikimedia.org/wikipedia/commons/d/d7/ObjectAdapter.png)

---        
# Adapter Pattern in C++*
.notes: [from sourcemaking.com](http://sourcemaking.com/design_patterns/adapter/cpp/1)
	
    !cpp
    #include <iostream.h>
    typedef int Coordinate;
    typedef int Dimension;

    // Desired interface
    class Rectangle{
    public:
        virtual void draw() = 0;
    };
    // Legacy component
    class LegacyRectangle{
    public:
        LegacyRectangle(Coordinate x1, Coordinate y1, Coordinate x2, Coordinate y2) {
            x1_ = x1; y1_ = y1;
            x2_ = x2; y2_ = y2;
            cout << "LegacyRectangle:  create.  (" << x1_ << "," << y1_ << ") => ("
                 << x2_ << "," << y2_ << ")" << endl;
        }
        void oldDraw() {
            cout << "LegacyRectangle:  oldDraw.  (" << x1_ << "," << y1_ << ") => (" 
                 << x2_ << "," << y2_ << ")" << endl;
        }
     private:
         Coordinate x1_; Coordinate y1_;
         Coordinate x2_; Coordinate y2_;
    };

---        
# Adapter Pattern in C++*
.notes: [from sourcemaking.com](http://sourcemaking.com/design_patterns/adapter/cpp/1)
	
    !cpp
    // Adapter wrapper
    class RectangleAdapter: public Rectangle, private LegacyRectangle {
    public:
        RectangleAdapter(Coordinate x, Coordinate y, 
            Dimension w, Dimension h): LegacyRectangle(x, y, x + w, y + h)
        {
        cout << "RectangleAdapter: create.(" << x << "," << y <<; "), width = " << w 
             << ", height = " << h << endl;
        }
        virtual void draw(){
            cout << "RectangleAdapter: draw." << endl;
            oldDraw();
        }
    };
    int main(){
        Rectangle *r = new RectangleAdapter(120, 200, 60, 40);
        r->draw();
    }

LegacyRectangle’s interface is not compatible with the system that would like to reuse it. An abstract base class is created that specifies the desired interface. An “adapter” class is defined that publicly inherits the interface of the abstract class, and privately inherits the implementation of the legacy component. This adapter class “maps” the new interface to the old implementation.

---        
# Adapter Pattern in Python*
.notes: [from The GITS Blog](http://ginstrom.com/scribbles/2009/03/27/the-adapter-pattern-in-python/)

We need to code a Person class, and a click_creature() function that will return a person's name and speech bubble to the GUI team.

    !python
    class Person(object):
        """A representation of a person in 2D Land"""
        def __init__(self, name):
            self.name = name
        def make_noise(self):
            return "hello"
        def click_creature(creature):
            """
            React to a click by retrieving the creature's
            name and what is says
            """
            return creature.name, creature.make_noise()

After, we find that our users are so happy with 2D Land that management has decided to build 2D Land version 2.0! The killer new feature of version 2.0 is that in addition to people, 2D Land will now also have dogs.

---
# Adapter Pattern in Python*
.notes: [from The GITS Blog](http://ginstrom.com/scribbles/2009/03/27/the-adapter-pattern-in-python/)

Luckily, the programmers over in the Pet Land team are going to let us use their Dog class. The Dog class has a name property, and a bark() method that returns the dog's bark sound.

    !python
    class Dog(object):
        """A representation of a dog in 2D Land"""
        def __init__(self, name):
            self.name = name
        def bark(self):
            return "woof"

As it stands, the Dog class doesn't have the interface that our clients expect. It has a name property, but instead of a make_noise() method, it's got a bark() method. But we can use the Adapter pattern to wrap the Dog class and give it the expected interface.

---

# Adapter Pattern in Python*
.notes: [from The GITS Blog](http://ginstrom.com/scribbles/2009/03/27/the-adapter-pattern-in-python/)

    !python
    from dog import Dog
    class Creature(object):
        """The base class for creatures in 2D Land"""
        def make_noise(self):
            """This is a technique to fake an ABC in Python 2.X"""
            raise NotImplementedError

    class Person(Creature):
        """A representation of a person in 2D Land"""
        def __init__(self, name):
            self.name = name

        def make_noise(self):
            return "hello"

    class DogClassAdapter(Creature, Dog):
        """Adapts the Dog class through multiple inheritance"""
        def __init__(self, name):
            Dog.__init__(self, name)

        def make_noise(self):
            """Provide the 'make_noise' method that the client expects"""
            return self.bark()

---     
# Decorator Pattern

The **Decorator** Pattern is used to extend the functionality of an object while maintaining its interface. The goal is to attach additional responsibilities to an object dynamically. Decorators provide a flexible alternative to subclassing for extending functionality. 

How does this work?

* You have an instance, and you put another instance inside of it. They both support the same (or similar) interfaces. The one on the outside is a **Decorator**.
* You use the one on the outside. It either masks, changes, or pass-throughs the methods of the instance inside of it.

---     
# Decorator Pattern

![Decorator Pattern](http://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Decorator_UML_class_diagram.svg/500px-Decorator_UML_class_diagram.svg.png)

---     
# Decorator Pattern in C++

    !cpp
    #include <iostream>
    using namespace std;

    // 1. "lowest common denom"
    class Widget{
      public:
        virtual void draw() = 0;
    };
    class TextField: public Widget{
        // 3. "Core" class & "is a"
        int width, height;
      public:
        TextField(int w, int h){
            width = w;
            height = h;
        }
        /*virtual*/
        void draw(){
            cout << "TextField: " << width << ", " << height << '\n';
        }
    };

---
# Decorator Pattern in C++

    !cpp
    // 2. 2nd level base class
    class Decorator: public Widget  // 3. "is a" relationship
    { 
        Widget *wid; // 4. "has a" relationship
      public:
        Decorator(Widget *w)
        {
            wid = w;
        }
        /*virtual*/
        void draw() 
        {
            wid->draw(); // 5. Delegation
        }
    };

---
# Decorator Pattern in C++

    !cpp
    class BorderDecorator: public Decorator
    {
      public:
        // 6. Optional embellishment
        BorderDecorator(Widget *w): Decorator(w){}
        /*virtual*/
        void draw()
        {
            // 7. Delegate to base class and add extra stuff
            Decorator::draw();  
            cout << "   BorderDecorator" << '\n';
        }
    };
    class ScrollDecorator: public Decorator
    {
      public:
        // 6. Optional embellishment
        ScrollDecorator(Widget *w): Decorator(w){}
        void draw()
        {
            // 7. Delegate to base class and add extra stuff
            Decorator::draw();
            cout << "   ScrollDecorator" << '\n';
        }
    };

---
# Decorator Pattern in C++

    !cpp
    int main()
    {
        // 8. Client has the responsibility to compose desired configurations
        Widget *aWidget = new BorderDecorator(
                             new BorderDecorator(
                                new ScrollDecorator(
                                   new TextField(80, 24)
                          )));
        aWidget->draw();
    }

---
# Decorator Pattern in Python*
.notes:[Decorator pattern from the Web](http://therning.org/magnus/archives/301)

    !python
    class Writer(object):
        def write(self, s):
            print s

    class WriterDecorator(object):
        def __init__(self, wrappee):
            self.wrappee = wrappee

        def write(self, s):
            self.wrappee.write(s)

    class UpperWriter(WriterDecorator):
        def write(self, s):
            self.wrappee.write(s.upper())

    class ShoutWriter(WriterDecorator):
        def write(self, s):
            self.wrappee.write('!'.join([t for t in s.split(' ') if t]) + '!')

    class YahooWriter(WriterDecorator):
        def __init__(self, wrappee):
            self.wrappee = UpperWriter(ShoutWriter(wrappee))

---
# Decorator Pattern in Python

    !python
    > w = Writer()
    > w.write('hello')
    hello
    > wd = WriterDecorator(w)
    > wd.write('hello')
    hello    
    > uw = UpperWriter(w)
    > uw.write('hello')
    HELLO
    > sw1 = ShoutWriter(w)
    > sw1.write('hello again')
    hello!again!
    > sw2 = ShoutWriter(uw)
    > sw2.write('hello again')
    HELLO!AGAIN!
    > yw = YahooWriter(w)
    > yw.write('hello again')
    HELLO!AGAIN!

---

# Strategy Pattern

The **Strategy** pattern (also known as the policy pattern) is a particular software design pattern, whereby algorithms can be selected at runtime. Formally speaking, the strategy pattern defines a family of algorithms, encapsulates each one, and makes them interchangeable. Strategy lets the algorithm vary independently from clients that use it.

The goals of this pattern are:

* Define a family of algorithms, encapsulate each one, and make them interchangeable. Strategy lets the algorithm vary independently from the clients that use it.
* Capture the abstraction in an interface, bury implementation details in derived classes.

---
# Strategy Pattern

![Strategy Pattern diagram](http://upload.wikimedia.org/wikipedia/commons/thumb/0/08/StrategyPatternClassDiagram.svg/500px-StrategyPatternClassDiagram.svg.png)

---
# Strategy Pattern*
.notes:[Wikibooks C++ Patterns](http://en.wikibooks.org/wiki/C++_Programming/Code/Design_Patterns#Strategy)

    !cpp
    #include <iostream>
    using namespace std;
 
    class StrategyInterface{
        public:
            virtual void execute() const = 0;
    };
 
    class ConcreteStrategyA: public StrategyInterface{
        public:
            virtual void execute() const
            {
                cout << "Called ConcreteStrategyA execute method" << endl;
            }
    };

    class ConcreteStrategyB: public StrategyInterface{
        public:
            virtual void execute() const
            {
                cout << "Called ConcreteStrategyB execute method" << endl;
            }
    };

---
# Strategy Pattern

    !cpp
    class ConcreteStrategyC: public StrategyInterface{
        public:
            virtual void execute() const
            {
                cout << "Called ConcreteStrategyC execute method" << endl;
            }
    };
 
    class Context{
        private:
            StrategyInterface * strategy_;
        public:
            explicit Context(StrategyInterface *strategy):strategy_(strategy) { }
 
        void set_strategy(StrategyInterface *strategy)
        {
            strategy_ = strategy;
        }
 
        void execute() const
        {
            strategy_->execute();
        }
    };

---
# Strategy Pattern

    !cpp
    int main(int argc, char *argv[])
    {
        ConcreteStrategyA concreteStrategyA;
        ConcreteStrategyB concreteStrategyB;
        ConcreteStrategyC concreteStrategyC;
 
        Context contextA(&concreteStrategyA);
        Context contextB(&concreteStrategyB);
        Context contextC(&concreteStrategyC);
 
        contextA.execute(); // output: "Called ConcreteStrategyA execute method"
        contextB.execute(); // output: "Called ConcreteStrategyB execute method"
        contextC.execute(); // output: "Called ConcreteStrategyC execute method"
 
        contextA.set_strategy(&concreteStrategyB);
        contextA.execute(); // output: "Called ConcreteStrategyB execute method"
        contextA.set_strategy(&concreteStrategyC);
        contextA.execute(); // output: "Called ConcreteStrategyC execute method"
 
        return 0;
    }

---

# Chain of Responsibility Pattern

The **Chain-of-Responsibility** pattern is a design pattern consisting of a source of command objects and a series of processing objects. Each processing object contains a set of logic that describes the types of command objects that it can handle, and how to pass off those that it cannot handle to the next processing object in the chain. A mechanism also exists for adding new processing objects to the end of this chain.

In a variation of the standard **Chain-of-Responsibility** model, some handlers may act as dispatchers, capable of sending commands out in a variety of directions, forming a tree of responsibility. In some cases, this can occur recursively, with processing objects calling higher-up processing objects with commands that attempt to solve some smaller part of the problem; in this case recursion continues until the command is processed, or the entire tree has been explored. An XML interpreter (parsed, but not yet executed) might be a fitting example.

This pattern promotes the idea of loose coupling, which is considered a programming best practice.

The motivation behind **Chain-of-Responsibility** pattern is to create a system that can serve different requests in a hierarchical manner. In other words if an object that is a part of the system does not know how to respond to the given request it passes it along the object tree. As the name implies, each object along the route of the request can take the responsibility and serve the request.

---
# Chain of Responsibility Pattern*
.notes:[From Net Objectives](http://www.netobjectives.com/PatternRepository/index.php?title=TheChainOfResponsibilityPattern)

![Chain of Responsibility Pattern](http://www.netobjectives.com/PatternRepository/images/0/09/CoR_1.jpg)

---
# Chain of Responsibility Pattern*
.notes:[From Net Objectives](http://www.netobjectives.com/PatternRepository/index.php?title=TheChainOfResponsibilityPattern)

Each entity will be given 'a chance' to handle the request until one of them elects to handle it. Once the request is handled, no further entities are given a chance to handle it. Any entity will self-select (or not), and therefore the selection is done from the point of view of the entity.

A clear procedural analog to this is the switch/case statement or other multi-part branching logic, where the issue that determines the proper branch is relevant to the algorithm itself in each case, rather than a client issue (see Strategy):

	!cpp
	switch(condition) {

	     case A:
	     // Behavior A

	     case B
	     // Behavior B

	     case C:
	     // Behavior C

	     default:
	     // Default Behavior
	}
---
# Chain of Responsibility Pattern

	!python
	class Car:
	    def __init__(self):
	        self.name = None
	        self.km = 11100
	        self.fuel = 5
	        self.oil = 5

	def handle_fuel(car):
	    if car.fuel < 10:
	        print "added fuel"
	        car.fuel = 100

	def handle_km(car):
	    if car.km > 10000:
	        print "made a car test."
	        car.km = 0

	def handle_oil(car):
	    if car.oil < 10:
	        print "Added oil"
	        car.oil = 100

---
# Chain of Responsibility Pattern

	!python
	class Garage:
	    def __init__(self):
	        self.handlers = []

	    def add_handler(self, handler):
	        self.handlers.append(handler)

	    def handle_car(self, car):
	        for handler in self.handlers:
	            handler(car)

	if __name__ == '__main__':
	    handlers = [handle_fuel, handle_km, handle_oil]
	    garage = Garage()

	    for handle in handlers:
	        garage.add_handler(handle)
	    garage.handle_car(Car())

---
# Chain of Responsibility Pattern C++

	!cpp
	#include <iostream>
	#include <vector>
	#include <ctime>
	using namespace std;

	class Base
	{
	    Base *next; // 1. "next" pointer in the base class
	  public:
	    Base() {
	        next = 0;
	    }
	    void setNext(Base *n) {
	        next = n;
	    }
	    void add(Base *n) {
	        if (next)
	            next->add(n);
	        else
	            next = n;
	    }
	    // 2. The "chain" method in the base class always delegates to the next obj
	    virtual void handle(int i) {
	        next->handle(i);
	    }
	};

---
# Chain of Responsibility Pattern C++

    !cpp
    class Handler1: public Base
    {
        public:
            void handle(int i)
            {
                if (rand() % 3) {
                    // 3. Don't handle requests 3 times out of 4
                    cout << "H1 passsed " << i << "  ";
                    Base::handle(i); // 3. Delegate to the base class
                } else
                    cout << "H1 handled " << i << "  ";
                }
            };

    class Handler2: public Base
    {
        public:
            void handle(int i) {
                if (rand() % 3)
                {
                    cout << "H2 passsed " << i << "  ";
                    Base::handle(i);
                } else
                    cout << "H2 handled " << i << "  ";
            }
     };

---
# Chain of Responsibility Pattern C++

    !cpp
    class Handler3: public Base{
        public:
            void handle(int i) {
                if (rand() % 3) {
                    cout << "H3 passsed " << i << "  ";
                    Base::handle(i);
                } else
                    cout << "H3 handled " << i << "  ";
            }
    };

    int main() {
        srand(time(0));
        Handler1 root;
        Handler2 two;
        Handler3 thr;
        
        root.add(&two);
        root.add(&thr);
        thr.setNext(&root);
        
        for (int i = 1; i < 10; i++) {
            root.handle(i);
            cout << '\n';
        }
    }

